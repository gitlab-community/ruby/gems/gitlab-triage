# frozen_string_literal: true

RSpec.shared_context 'with project stubs context' do
  let(:project_id) { 123 }
  let(:fork_project_id) { 456 }
end
