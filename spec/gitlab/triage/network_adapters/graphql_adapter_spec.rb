# frozen_string_literal: true

require 'spec_helper'
require 'net/http'
require 'gitlab/triage/network_adapters/graphql_adapter'

describe Gitlab::Triage::NetworkAdapters::GraphqlAdapter do
  include_context 'with network context'

  let(:graphql_adapter) { described_class.new(network_options) }

  describe '#query' do
    let(:client) { instance_double(GraphQL::Client, query: response) }
    let(:errors) { [] }
    let(:response_data) { nil }
    let(:response) do
      double(errors: errors, data: response_data, extensions: { 'headers' => { 'ratelimit-remaining' => 600, 'ratelimit-reset' => 1604942574 } })
    end

    subject(:query) { graphql_adapter.query(:graphql_query, resource_path: [:project, :issues], variables: { source: 'gitlab-org/gitlab' }) }

    before do
      allow(graphql_adapter).to receive(:client).and_return(client)
    end

    it 'sends request through GraphQL::Client' do
      expect(client).to receive(:query).with(:graphql_query, { variables: { source: 'gitlab-org/gitlab' }, context: { token: network_options.token } })
      query
    end

    context 'when response has errors' do
      let(:errors) { double(blank?: false, messages: { text: 'GraphQL Error' }) }

      it 'raises error' do
        expect { query }.to raise_error(RuntimeError, 'There was an error: {"text":"GraphQL Error"}')
      end
    end

    context 'when response does not match resource path' do
      it 'does not raise error' do
        expect { query }.not_to raise_error
      end

      it { is_expected.to eq(results: {}, ratelimit_remaining: 600, ratelimit_reset_at: Time.at(1604942574)) }
    end

    context 'when response matches resource path' do
      let(:response_data) { double(project: double(issues: response_object)) }

      context 'when response is a GraphQL connection' do
        let(:response_object) { double(nodes?: true, nodes: [double(to_h: { id: 1 })], page_info: double(has_next_page: false, end_cursor: 'END')) }

        it 'does not raise error' do
          expect { query }.not_to raise_error
        end

        it 'returns results with ratelimit values' do
          expect(query).to eq(
            results: [{ id: 1 }],
            more_pages: false,
            end_cursor: 'END',
            ratelimit_remaining: 600,
            ratelimit_reset_at: Time.at(1604942574)
          )
        end
      end

      context 'when response is a GraphQL::Client::List' do
        let(:response_object) { GraphQL::Client::List.new([{ id: 1 }]) }

        it 'does not raise error' do
          expect { query }.not_to raise_error
        end

        it { is_expected.to eq(results: [{ id: 1 }], ratelimit_remaining: 600, ratelimit_reset_at: Time.at(1604942574)) }
      end

      context 'when response is a simple hash' do
        let(:response_object) { double(nodes?: false, to_h: { id: 1 }) }

        it 'does not raise error' do
          expect { query }.not_to raise_error
        end

        it { is_expected.to eq(results: { id: 1 }, ratelimit_remaining: 600, ratelimit_reset_at: Time.at(1604942574)) }
      end
    end
  end
end
