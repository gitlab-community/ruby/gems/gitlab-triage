## Developer Certificate of Origin + License

By contributing to GitLab Inc., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab Inc.
Except for the license granted herein to GitLab Inc. and recipients of software
distributed by GitLab Inc., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

## Style guide

This project employs [rubocop](https://github.com/bbatsov/rubocop) to enforce code styles. Check your new branch by:

```
git checkout my_branch
bundle install --path vendor/bundle
bundle exec rubocop
```

## Testing

- Please consider tests for any code change that is made
- Please ensure that the existing test suite passes

```
git checkout my_branch
bundle install --path vendor/bundle
bundle exec rake spec
```
